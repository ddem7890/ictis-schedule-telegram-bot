# -*- coding: utf-8 -*-
import requests
import argparse
import json
import os
import telegram
import uuid
from telegram.ext import (Updater,
                          CommandHandler,
                          MessageHandler,
                          CallbackQueryHandler,
                          Filters)
from telegram import (InlineKeyboardButton,
                      InlineKeyboardMarkup)
import logging
from weasyprint import HTML, CSS

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)


def get_timetable_text(response_data):
    """Create timetable from dict and
    return message for user.

    Parameters
    ----------
    response_data: dict
        dict gotten from json response from ictis-schedule API

    Returns
    -------
    str
        Message for user

    """
    table = response_data['table']

    title = f'{table["type"]} ' \
            f'{table["name"]} ' \
            f'на {table["week"]} неделю'

    response_msg = f'<b>{title}</b>'
    days = table['table'][2:]
    times = table['table'][1]
    for day in days:
        response_msg += f'\n<b>{day[0]}</b>\n'
        for i, pair in enumerate(day[1:]):
            response_msg += f'<b>{i+1}.</b> {times[i + 1]}: ' + \
                            (f'{pair}' if len(pair) > 0 else 'окно') + '\n'

    return response_msg


def get_timetable_filename(response_data):
    """Create timetable image from dict

    Parameters
    ----------
    response_data: dict
        dict gotten from json response from ictis-schedule API

    Returns
    -------
    str
        filename with response

    """
    table = response_data['table']

    title = f'{table["type"]} ' \
            f'{table["name"]} ' \
            f'на {table["week"]} неделю'

    response_msg = '<meta charset="UTF-8" />'
    response_msg += f'<h3>{title}</h3>\n'

    response_msg += '<table>'
    for line in table["table"]:
        response_msg += '<tr>'
        for col in line:
            col = col.replace(', ', ',')
            col = col.replace(',', ', ')
            response_msg += f'<td>{col}</td>'
        response_msg += '</tr>'
    response_msg += '</table>'

    filename = f'{uuid.uuid4()}.jpg'

    css = CSS(
        string="""
                @page {
                    size: 400mm 300mm;
                }
                table, th, td {
                     border: 1px solid black;
                }
        """
    )
    HTML(string=response_msg).write_png(filename, stylesheets=[css])

    return filename


def create_choices_response(bot, chat_id, response_data):
    """Give to user choice of timetables

    Parameters
    ----------
    bot: telegram.Bot
        Bot object, which send message
    chat_id: int
        ID of chat with user
    response_data: dict
        dict gotten from json response from ictis-schedule API

    """
    buttons = []
    choices = response_data['choices']
    for choice in choices:
        name = choice['name']
        group = choice['group']

        button = InlineKeyboardButton(name,
                                      callback_data=json.dumps({
                                          "g": group
                                      }))
        buttons.append(button)

    keyboard = [[]]
    for btn in buttons:
        if len(keyboard[-1]) == 3:
            keyboard.append([])
        keyboard[-1].append(btn)

    response_menu = InlineKeyboardMarkup(keyboard,
                                         force_reply=True,
                                         selective=True)
    response_msg = 'Выберите расписание'

    bot.send_message(chat_id,
                     response_msg,
                     reply_markup=response_menu,
                     parse_mode=telegram.ParseMode.HTML)


def create_timetable_response(bot, chat_id, response_data, send_text=False):
    """Send timetable to user

    Parameters
    ----------
    bot: telegram.Bot
        Bot object, which send message
    chat_id: int
        ID of chat with user
    response_data: dict
        dict gotten from json response from ictis-schedule API
    send_text: bool
        if True, send text, else image

    """
    table = response_data["table"]

    buttons = [InlineKeyboardButton(
        str(week),
        callback_data=json.dumps({
            "g": table["group"],
            "w": week
        })) for week in response_data['weeks']]
    keyboard = [[]]
    for btn in buttons:
        if len(keyboard[-1]) == 8:
            keyboard.append([])
        keyboard[-1].append(btn)
    response_menu = InlineKeyboardMarkup(keyboard)

    if send_text:
        response_msg = get_timetable_text(response_data)
        bot.send_message(chat_id,
                         response_msg,
                         reply_markup=response_menu,
                         parse_mode=telegram.ParseMode.HTML)

    else:
        filename = get_timetable_filename(response_data)

        bot.send_photo(chat_id,
                       open(filename, 'rb'),
                       reply_markup=response_menu,
                       parse_mode=telegram.ParseMode.HTML)

        os.remove(filename)


def do_start(update, context):
    """Process start command

    """
    chat_id = update.effective_chat.id
    first_name = update.effective_chat.first_name
    response = f'Бот <b>"Расписание ИКТИБ"</b> приветствует тебя, ' \
               f'<b>{first_name}</b>!\n' \
               f'Используй /help для ознакомления с инструкцией.'

    logging.info(f'Start chat: {chat_id}')

    context.bot.send_message(chat_id,
                             response,
                             parse_mode=telegram.ParseMode.HTML)


def do_help(update, context):
    """Process start command

    """
    chat_id = update.effective_chat.id
    response = f'<b>Инструкции</b>\n' \
               f'1. Отправь название группы, аудиторию или фамилию ' \
               f'преподавателя для поиска расписания.\n' \
               f'2. Я могу попросить уточнить раписание. \n' \
               f'3. Если я ничего не найду, то скажу об этом. \n' \
               f'4. Расписание я присылаю на текущую неделю. ' \
               f'Если нужна другая неделя, нажми соответсвующую кнопку.'

    logging.info(f'Help: {chat_id}')

    context.bot.send_message(chat_id,
                             response,
                             parse_mode=telegram.ParseMode.HTML)


def process_message(update, context):
    """Process message request from user.

    """
    message = update.message.text
    chat_id = update.effective_chat.id
    bot = context.bot
    logging.info(f'Request from {chat_id}: {message}')

    try:
        request = f'http://ictis.sfedu.ru/schedule-api/?query={message}'
        response = requests.get(url=request)
        response_data = response.json()

        if 'choices' in response_data:
            create_choices_response(bot, chat_id, response_data)
        elif 'table' in response_data:
            create_timetable_response(bot, chat_id, response_data)
        else:
            response_msg = "Я ничего не нашёл -_-"
            bot.send_message(chat_id,
                             response_msg)
    except Exception as ex:
        ex.with_traceback()
        bot.send_message(chat_id,
                         'В этот раз я ошибся =(')


def process_button(update, context):
    """Process user's button click

    """
    bot = context.bot
    query = update.callback_query
    request_data = json.loads(query.data)
    chat_id = update.effective_chat.id
    logging.info(f'Request from {chat_id}: {request_data}')

    try:
        if 'w' in request_data:
            group = request_data['g']
            week = request_data['w']
            request = f'http://ictis.sfedu.ru/schedule-api/' \
                      f'?group={group}&week={week}'
        else:
            group = request_data['g']
            request = f'http://ictis.sfedu.ru/schedule-api' \
                      f'/?group={group}'

        response = requests.get(url=request)
        response_data = response.json()

        create_timetable_response(bot, chat_id, response_data)
    except Exception as ex:
        ex.with_traceback(ex.__traceback__)
        bot.send_message(chat_id,
                         'В этот раз я ошибся =(')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('token',
                        type=str,
                        help='Token for your bot')
    parser.add_argument('--pooling',
                        action='store_true',
                        help='If True, use long pooling, else webhook')
    parser.add_argument('--url',
                        type=str,
                        default=None,
                        help='Web server url')
    parser.add_argument('--port',
                        type=int,
                        default=8443,
                        help='Port')
    args = parser.parse_args()
    token = args.token
    logging.info('Parsing args has been finished')

    updater = Updater(token=token, use_context=True)
    dispatcher = updater.dispatcher

    start_handler = CommandHandler('start', do_start)
    dispatcher.add_handler(start_handler)

    help_handler = CommandHandler('help', do_help)
    dispatcher.add_handler(help_handler)

    message_handler = MessageHandler(Filters.text, process_message)
    dispatcher.add_handler(message_handler)

    button_handler = CallbackQueryHandler(process_button)
    dispatcher.add_handler(button_handler)

    logging.info('Creating handlers has been finished')

    if args.pooling:
        updater.start_polling()
    else:
        updater.start_webhook(listen="0.0.0.0",
                              port=args.port,
                              url_path=token)
        updater.bot.setWebhook(f'{args.url}/{token}')
        updater.idle()


if __name__ == "__main__":
    main()
