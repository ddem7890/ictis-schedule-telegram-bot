# ictis-schedule-telegram-bot

### Get start

1. Install requirements.
```bash
pip install -r requirements.txt
```
2. Start bot for long pooling
```bash
python bot.py <token> --pooling
```
### Usage
Start:

![/start](images/start.png "Start")

Make request and schedule choice:

![Schedule choice](images/choice.png "Schedule choice")

Get timetable:

![Schedule choice](images/timetable.png "Schedule choice")

Fail sample:

![Fail](images/fail.png "Fail")